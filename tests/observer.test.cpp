/*************************************************************************************************************

 mvd streams


 Copyright 2019 mvd

 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 compliance with the License. You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software distributed under the License is
 distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and limitations under the License.

*************************************************************************************************************/

#include <catch2/catch.hpp>

#include <observer.h>
#include <access_policy.h>

#include <future>
#include <random>

namespace mvd
{

  TEST_CASE( "observable_base (non-locking)" )
  {
    using observer_t = observer_base < access_policy::none >;
    using observable_t = observable_base < access_policy::none >;
  
    SECTION( "Registering observer increases registered count" )
    {
      observer_t o1, o2;
      observable_t observable;
    
      REQUIRE( observable.get_observer_count() == 0 );
    
      observable.register_observer(o1);
      CHECK( observable.get_observer_count() == 1 );
    
      observable.register_observer(o2);
      CHECK( observable.get_observer_count() == 2 );
    }
  
    SECTION( "Unregistering observer decreases registered count" )
    {
      observer_t o1, o2;
      observable_t observable;
    
      observable.register_observer(o1);
      observable.register_observer(o2);
    
      REQUIRE( observable.get_observer_count() == 2 );
    
      observable.unregister_observer(o1);
      CHECK( observable.get_observer_count() == 1 );
    
      observable.unregister_observer(o2);
      CHECK( observable.get_observer_count() == 0 );
    }
  
    SECTION( "Unregistering unknown observer has no effect" )
    {
      observer_t o1, o2;
      observable_t observable;
    
      observable.register_observer(o1);
    
      REQUIRE( observable.get_observer_count() == 1 );
    
      observable.unregister_observer(o2);
      CHECK( observable.get_observer_count() == 1 );
    }
  }

  TEST_CASE( "observable_base (locking)" )
  {
    using observer_t = observer_base < access_policy::locked >;
    using observable_t = observable_base < access_policy::locked >;
    
    SECTION( "Concurrent subscription / unsubscription" )
    {
      auto observers = std::vector< observer_t >( 20 );
      observable_t observable;
    
      auto seed = std::random_device()();
      INFO( "Random seed used: " << seed );
      std::mt19937 gen( seed );
      std::uniform_int_distribution<> dist(1, 10);
 
      auto tasks = std::vector< std::future< void > >();
      for( auto& o : observers )
      {
        tasks.push_back( std::async(
          std::launch::async,
          [&o, &observable, &dist, &gen]()
          {
            for( size_t i = 0; i < 100; ++i )
            {
              observable.register_observer( o );
              std::this_thread::sleep_for( std::chrono::milliseconds( dist( gen ) ) );
              observable.unregister_observer( o );
              std::this_thread::sleep_for( std::chrono::milliseconds( dist( gen ) ) );
            }
          }
        ));
      }
      tasks.clear();
    
      CHECK( observable.get_observer_count() == 0 );
    }
  }
}
